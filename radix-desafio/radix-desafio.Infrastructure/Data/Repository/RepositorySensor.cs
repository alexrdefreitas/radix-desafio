﻿using radix_desafio.Domain.Core.Interface.Repository;
using radix_desafio.Domain.Entity;

namespace radix_desafio.Infrastructure.Data.Repository
{
    public class RepositorySensor : RepositoryBase<Sensor>, IRepositorySensor
    {
        private readonly SqlContext sqlContext;
        public RepositorySensor(SqlContext sqlContext) : base(sqlContext)
        {
            this.sqlContext = sqlContext;
        }
    }
}
