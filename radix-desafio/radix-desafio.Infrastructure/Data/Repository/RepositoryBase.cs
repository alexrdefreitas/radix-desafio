﻿using Microsoft.EntityFrameworkCore;
using radix_desafio.Domain.Core.Interface.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace radix_desafio.Infrastructure.Data.Repository
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        private readonly SqlContext sqlContext;

        public RepositoryBase(SqlContext sqlContext)
        {
            this.sqlContext = sqlContext;
        }

        public void Add(T objeto)
        {
            try
            {
                this.sqlContext.Set<T>().Add(objeto);
                this.sqlContext.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Delete(T objeto)
        {
            try
            {
                this.sqlContext.Set<T>().Remove(objeto);
                this.sqlContext.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<T> GetAll()
        {
            return this.sqlContext.Set<T>().ToList();
        }

        public T GetById(int id)
        {
            return this.sqlContext.Set<T>().Find(id);
        }

        public void Update(T objeto)
        {
            try
            {
                this.sqlContext.Entry(objeto).State = EntityState.Modified;
                this.sqlContext.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
