﻿using Microsoft.EntityFrameworkCore;
using radix_desafio.Domain.Entity;

namespace radix_desafio.Infrastructure.Data
{
    public class SqlContext : DbContext
    {
        public SqlContext()
        {

        }

        public SqlContext(DbContextOptions<SqlContext> options) : base(options) { }

        public DbSet<Sensor> sensor { get; set; }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
