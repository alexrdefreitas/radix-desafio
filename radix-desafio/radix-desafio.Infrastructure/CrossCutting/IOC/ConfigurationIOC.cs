﻿using Autofac;
using radix_desafio.Application;
using radix_desafio.Application.Interface;
using radix_desafio.Application.Interface.Mapper;
using radix_desafio.Application.Mappers;
using radix_desafio.Domain.Core.Interface.Repository;
using radix_desafio.Domain.Core.Interface.Service;
using radix_desafio.Domain.Service;
using radix_desafio.Infrastructure.Data.Repository;

namespace radix_desafio.Infrastructure.CrossCutting.IOC
{
    public class ConfigurationIOC
    {
        public static void Load(ContainerBuilder builder)
        {
            #region IOC

            builder.RegisterType<ApplicationServiceSensor>().As<IApplicationServiceSensor>();
            builder.RegisterType<ServiceSensor>().As<IServiceSensor>();
            builder.RegisterType<RepositorySensor>().As<IRepositorySensor>();
            builder.RegisterType<MapperSensor>().As<IMapperSensor>();

            #endregion
        }
    }
}
