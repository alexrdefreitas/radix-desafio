﻿using radix_desafio.Domain.Core.Interface.Repository;
using radix_desafio.Domain.Core.Interface.Service;
using radix_desafio.Domain.Entity;

namespace radix_desafio.Domain.Service
{
    public class ServiceSensor : ServiceBase<Sensor>, IServiceSensor
    {
        private readonly IRepositorySensor repositorySensor;

        public ServiceSensor(IRepositorySensor repositorySensor) : base(repositorySensor)
        {
            this.repositorySensor = repositorySensor;
        }
    }
}
