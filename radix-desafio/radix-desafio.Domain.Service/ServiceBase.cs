﻿using radix_desafio.Domain.Core.Interface.Repository;
using radix_desafio.Domain.Core.Interface.Service;
using System.Collections.Generic;

namespace radix_desafio.Domain.Service
{
    public class ServiceBase<T> : IServiceBase<T> where T : class
    {
        private readonly IRepositoryBase<T> repositoryBase;

        public ServiceBase(IRepositoryBase<T> repositoryBase)
        {
            this.repositoryBase = repositoryBase;
        }
        public void Add(T objeto)
        {
            this.repositoryBase.Add(objeto);
        }

        public void Delete(T objeto)
        {
            this.repositoryBase.Delete(objeto);
        }

        public IEnumerable<T> GetAll()
        {
            return this.repositoryBase.GetAll();
        }

        public T getById(int id)
        {
            return this.repositoryBase.GetById(id);
        }

        public void Update(T objeto)
        {
            this.repositoryBase.Update(objeto);
        }
    }
}
