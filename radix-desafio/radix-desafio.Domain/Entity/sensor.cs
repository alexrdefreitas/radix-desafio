﻿using System.Collections;
using System.Dynamic;

namespace radix_desafio.Domain.Entity
{
    public class Sensor : Base
    {
        public string timeStamp { get; set; }
        public string pais { get; set; }
        public string regiao { get; set; }
        public string sensor { get; set; }
        public string valor { get; set; }
        public bool numerico { get; set; }
    }
}
