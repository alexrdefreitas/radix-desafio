﻿using radix_desafio.Application.Interface;
using radix_desafio.Application.Interface.Mapper;
using radix_desafio.Application.VO;
using radix_desafio.Domain.Core.Interface.Service;
using System.Collections.Generic;

namespace radix_desafio.Application
{
    public class ApplicationServiceSensor : IApplicationServiceSensor

    {
        private readonly IServiceSensor serviceSensor;
        private readonly IMapperSensor mapperSensor;

        public ApplicationServiceSensor(IServiceSensor serviceSensor, IMapperSensor mapperSensor)
        {
            this.serviceSensor = serviceSensor;
            this.mapperSensor = mapperSensor;
        }

        public void Add(SensorVO sensor)
        {
            var entity = mapperSensor.MapperVOToEntity(sensor);
            serviceSensor.Add(entity);
        }

        public void Delete(SensorVO sensor)
        {
            var entity = mapperSensor.MapperVOToEntity(sensor);
            serviceSensor.Delete(entity);
        }

        public IEnumerable<SensorVO> GetAll()
        {
            var entity = serviceSensor.GetAll();
            return mapperSensor.MapperListClientesVO(entity);
        }

        public SensorVO GetById(int id)
        {
            var entity = serviceSensor.getById(id);
            return mapperSensor.MapperEntityToVO(entity);
        }

        public void Update(SensorVO sensor)
        {
            var entity = mapperSensor.MapperVOToEntity(sensor);
            serviceSensor.Update(entity);
        }
    }
}
