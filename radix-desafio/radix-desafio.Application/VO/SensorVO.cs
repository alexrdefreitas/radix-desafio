﻿namespace radix_desafio.Application.VO
{
    public class SensorVO
    {
        public int? id { get; set; }
        public double timestamp { get; set; }
        public string tag { get; set; }
        public string valor { get; set; }
    }
}
