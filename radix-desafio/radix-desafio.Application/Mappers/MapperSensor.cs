﻿using radix_desafio.Application.Interface.Mapper;
using radix_desafio.Application.VO;
using radix_desafio.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace radix_desafio.Application.Mappers
{
    public class MapperSensor : IMapperSensor
    {
        IEnumerable<SensorVO> sensorVOs = new List<SensorVO>();
        public SensorVO MapperEntityToVO(Sensor sensor)
        {
            var sensorVO = new SensorVO()
            {
                id = sensor.id,
                timestamp = double.Parse(sensor.timeStamp),
                tag = $"{sensor.pais}.{sensor.regiao}.{sensor.sensor}",
                valor = sensor.valor
            };
            return sensorVO;
        }

        public IEnumerable<SensorVO> MapperListClientesVO(IEnumerable<Sensor> sensor)
        {
            var vo = sensor.Select(
                        c =>
                            new SensorVO
                            {
                                id = c.id,
                                timestamp = double.Parse(c.timeStamp),
                                tag = $"{c.pais}.{c.regiao}.{c.sensor}",
                                valor = c.valor
                            }

                        );

            return vo.ToList();
        }

        public Sensor MapperVOToEntity(SensorVO sensorVO)
        {
            if (string.IsNullOrEmpty(sensorVO.valor) || string.IsNullOrWhiteSpace(sensorVO.valor))
                throw new ArgumentNullException("Campo valor não pode ser vazio ou nulo");

            var tag = sensorVO.tag.Split(".");
            var isNumeric = int.TryParse(sensorVO.valor, out _);
            var sensor = new Sensor()
            {
                id = sensorVO.id,
                timeStamp = sensorVO.timestamp.ToString(),
                pais = tag[0],
                regiao = tag[1],
                sensor = tag[2],
                valor = sensorVO.valor,
                numerico = isNumeric
            };
            return sensor;

        }
    }
}
