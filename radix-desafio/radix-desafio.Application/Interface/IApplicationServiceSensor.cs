﻿using radix_desafio.Application.VO;
using System.Collections.Generic;

namespace radix_desafio.Application.Interface
{
    public interface IApplicationServiceSensor
    {
        void Add(SensorVO sensor);
        void Update(SensorVO sensor);
        void Delete(SensorVO sensor);
        IEnumerable<SensorVO> GetAll();
        SensorVO GetById(int id);
    }
}
