﻿using radix_desafio.Application.VO;
using radix_desafio.Domain.Entity;
using System.Collections.Generic;

namespace radix_desafio.Application.Interface.Mapper
{
    public interface IMapperSensor
    {
        Sensor MapperVOToEntity(SensorVO sensorVO);
        IEnumerable<SensorVO> MapperListClientesVO(IEnumerable<Sensor> sensor);
        SensorVO MapperEntityToVO(Sensor sensor);
    }
}
