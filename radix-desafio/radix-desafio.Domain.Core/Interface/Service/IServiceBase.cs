﻿using System.Collections.Generic;

namespace radix_desafio.Domain.Core.Interface.Service
{
    public interface IServiceBase<T> where T : class
    {
        void Add(T objeto);
        void Update(T objeto);
        void Delete(T objeto);
        IEnumerable<T> GetAll();
        T getById(int id);
    }
}
