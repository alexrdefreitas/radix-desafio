﻿using radix_desafio.Domain.Entity;

namespace radix_desafio.Domain.Core.Interface.Service
{
    public interface IServiceSensor : IServiceBase<Sensor>
    {
    }
}
