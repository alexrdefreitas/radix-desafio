﻿using radix_desafio.Domain.Entity;

namespace radix_desafio.Domain.Core.Interface.Repository
{
    public interface IRepositorySensor : IRepositoryBase<Sensor>
    {
    }
}
