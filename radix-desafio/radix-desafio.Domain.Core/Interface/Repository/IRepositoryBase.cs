﻿using System.Collections.Generic;

namespace radix_desafio.Domain.Core.Interface.Repository
{
    public interface IRepositoryBase<T> where T : class
    {
        void Add(T objeto);
        void Update(T objeto);
        void Delete(T objeto);
        IEnumerable<T> GetAll();
        T GetById(int id);

    }
}
