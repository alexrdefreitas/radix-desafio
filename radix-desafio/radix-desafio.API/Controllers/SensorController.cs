﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using radix_desafio.Application.Interface;
using radix_desafio.Application.VO;

namespace radix_desafio.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SensorController : ControllerBase
    {
        private readonly IApplicationServiceSensor _applicationServiceSensor;

        public SensorController(IApplicationServiceSensor ApplicationServiceSensor)
        {
            _applicationServiceSensor = ApplicationServiceSensor;
        }

        [HttpGet]
        [ProducesResponseType((200), Type = typeof(List<SensorVO>))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(_applicationServiceSensor.GetAll());
        }

        [HttpGet("{id}")]
        [ProducesResponseType((200), Type = typeof(List<string>))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public ActionResult<string> Get(int id)
        {
            return Ok(_applicationServiceSensor.GetById(id));
        }

        [HttpPost]
        [ProducesResponseType((200), Type = typeof(List<SensorVO>))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public IActionResult Post([FromBody] SensorVO sensorVO)
        {
            try
            {
                if (sensorVO == null)
                    return NotFound();

                _applicationServiceSensor.Add(sensorVO);
                return Ok("Sensor incluído com sucesso!");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPut]
        [ProducesResponseType((200), Type = typeof(List<string>))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public IActionResult Put([FromBody] SensorVO sensorVO)
        {
            try
            {
                if (sensorVO == null)
                    return NotFound();

                _applicationServiceSensor.Update(sensorVO);
                return Ok("Sensor alterado com sucesso!");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpDelete]
        [ProducesResponseType((200), Type = typeof(List<string>))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public IActionResult Delete([FromBody] SensorVO sensorVO)
        {
            try
            {
                if (sensorVO == null)
                    return NotFound();

                _applicationServiceSensor.Delete(sensorVO);
                return Ok("Sensor excluído com sucesso!");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


    }
}
